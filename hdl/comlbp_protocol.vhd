library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library comlbp_lib;

entity comlbp_protocol is
    port(
        aresetn                 : in std_logic;
        clk                     : in std_logic;

        pps                     : in std_logic;

        s_axis_tdata            : in std_logic_vector(31 downto 0);
        s_axis_tvalid           : in std_logic;

        sreset                  : in std_logic;
        enable                  : in std_logic;
        faseq_offset            : in signed(15 downto 0);
        packet_count            : out std_logic_vector(31 downto 0);
        packet_rate             : out std_logic_vector(23 downto 0);

        m_axis_tvalid           : out std_logic;
        m_axis_tdata_xpos       : out std_logic_vector(31 downto 0);
        m_axis_tdata_ypos       : out std_logic_vector(31 downto 0);
        m_axis_tdata_faseq      : out std_logic_vector(15 downto 0);
        m_axis_tdata_bpmid      : out std_logic_vector(15 downto 0)

    );
end entity comlbp_protocol;


architecture rtl of comlbp_protocol is


    type arr_slv is array (natural range <>) of std_logic_vector;

    constant C_R_LEN    : natural := 4;

    signal r_data       : arr_slv(0 to C_R_LEN-1)(31 downto 0);
    signal onehot_valid : std_logic_vector(C_R_LEN-1 downto 0);

    signal packet_valid : std_logic;
    signal packet_cnt   : unsigned(31 downto 0);

    signal packet_rate_cnt  : unsigned(23 downto 0);
    signal r_packet_rate    : unsigned(23 downto 0);


begin

    packet_count    <= std_logic_vector(packet_cnt);
    packet_rate     <= std_logic_vector(r_packet_rate);

    data_reg_p:process(aresetn, clk)
    begin
        if aresetn = '0'then
            r_data          <= (others => (others => '0'));
            onehot_valid    <= (0=> '1', others => '0');
            packet_valid    <= '0';
            packet_cnt      <= (others => '0');
            r_packet_rate   <= (others => '0');
            packet_rate_cnt <= (others => '0');
        elsif rising_edge(clk) then
            if sreset = '1' then
                onehot_valid    <= (0=> '1', others => '0');
                packet_valid    <= '0';
                packet_cnt      <= (others => '0');

            else

                -- Fill up words in register pipe
                if s_axis_tvalid = '1' then

                    -- rotate one hot
                    onehot_valid    <= onehot_valid(0)&onehot_valid(C_R_LEN-1 downto 1);

                    -- Data register fall down index to index toward 0
                    r_data(C_R_LEN-1)   <= s_axis_tdata;
                    for I in 1 to C_R_LEN-1 loop
                        r_data(I-1)     <= r_data(I);
                    end loop;
                end if;

                -- Signal valid when receiving last word
                packet_valid <= enable and s_axis_tvalid and onehot_valid(1);

                if packet_valid = '1' then
                    packet_cnt      <= packet_cnt+1;
                end if;

            end if;

            if pps = '1' then
                r_packet_rate   <= packet_rate_cnt;
                packet_rate_cnt <= (others => '0');
            else
                if packet_valid = '1' then
                    packet_rate_cnt <= packet_rate_cnt+1;
                end if;
            end if;
        end if;
    end process;

    -- Decoding
    -- The incoming order is SUM, X, Y, STATUS, set on the LBP
    m_axis_tvalid       <= packet_valid;
    m_axis_tdata_xpos   <= r_data(1);
    m_axis_tdata_ypos   <= r_data(2);
    m_axis_tdata_faseq  <= std_logic_vector(signed(r_data(3)(31 downto 16)) + faseq_offset);
    m_axis_tdata_bpmid  <= "0000000"&r_data(3)(10 downto 2);

end architecture;

