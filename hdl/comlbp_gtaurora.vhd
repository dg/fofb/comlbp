library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library UNISIM;
use UNISIM.vcomponents.all;

library xpm;
use xpm.vcomponents.all;

library comlbp_lib;
use comlbp_lib.pkg_comlbp.all;

entity comlbp_gtaurora is
    generic(
        G_IPNUM                            : natural :=  0
    );
    port(

        free_100_clk                       : in std_logic;
        aresetn                            : in std_logic;

        -- QPLL common
        qpll_clk_in                        : in std_logic;
        qpll_refclk_in                     : in std_logic;
        qpll_lock_in                       : in std_logic;
        qpll_reset_out                     : out std_logic;

        -- SFP signals
        sfp_rxn                            : in std_logic;
        sfp_rxp                            : in std_logic;
        sfp_txn                            : out std_logic;
        sfp_txp                            : out std_logic;

        -- AXIS
        m_axis_clk                         : out std_logic;
        m_axis_tdata                       : out std_logic_vector(31 downto 0);
        m_axis_tvalid                      : out std_logic;

        -- Status
        hard_err                           : out std_logic;
        soft_err                           : out std_logic;
        channel_up                         : out std_logic;
        lane_up                            : out std_logic;
        powergood                          : out std_logic;
        gtclk_active                       : out std_logic;
        aurora_datavalid                   : out std_logic;
        aurora_reset_lane                  : out std_logic;
        txpmarstdone                       : out std_logic;
        rxpmarstdone                       : out std_logic;
        gttxrstdone                        : out std_logic;
        gtwztxrstdone                      : out std_logic;
        gtrxrstdone                        : out std_logic;
        gtwzrxrstdone                      : out std_logic;
        tx_lock                            : out std_logic;
        gtcdrstable                        : out std_logic;
        word_count                         : out std_logic_vector(31 downto 0);

        -- Control
        gtloopback                         : in std_logic_vector(2 downto 0);
        usrrst_system                      : in std_logic;
        usrrst_gtwizreset                  : in std_logic;
        usrrst_gtreset                     : in std_logic

    );
end entity comlbp_gtaurora;

architecture struct of comlbp_gtaurora is

    ------------------------
    -- SIGNAL DECLARATION --
    ------------------------
    signal areset               : std_logic;
    -- Reset internal signals
    signal reg_rst_system       : std_logic_vector(6 downto 0);
    signal rst_datapath         : std_logic;
    signal rst_datapath_sp      : std_logic;    -- sync pulse to freeclk
    signal rst_auroragt         : std_logic;
    signal rst_aurorasys        : std_logic;
    signal rst_gtwiz            : std_logic;
    signal reg_gtreset          : std_logic_vector(6 downto 0);

    -- Reset signals from aurora
    signal gt_rxreset           : std_logic;
    signal gt_link_reset        : std_logic;

    -- Reset signals from gtwizard
    signal rst_tx_usrclk        : std_logic;
    signal gt_txpmarstdone      : std_logic;
    signal gt_rxpmarstdone      : std_logic;
    signal gt_txrstdone         : std_logic_vector(0 downto 0);
    signal gt_rxrstdone         : std_logic_vector(0 downto 0);


    -- Clocking signals
    signal txoutclk             : std_logic;
    signal clk_gt               : std_logic;
	signal reg_clk_active       : std_logic_vector(6 downto 0);

    -- Between aurora and GT
    signal gt_txdata            : std_logic_vector(31 downto 0);
    signal gt_rxdata            : std_logic_vector(31 downto 0);
    signal gt_rxpolarity        : std_logic_vector(0 downto 0);
    signal gt_rxrealign         : std_logic_vector(0 downto 0);
    signal gt_enacommaalign     : std_logic_vector(0 downto 0);
    signal gt_rxbufstatus       : std_logic_vector(2 downto 0);
    signal gt_txbufstatus       : std_logic_vector(1 downto 0);
    signal gt_rxctrl0           : std_logic_vector(15 downto 0);
    signal gt_rxctrl1           : std_logic_vector(15 downto 0);
    signal gt_rxctrl2           : std_logic_vector(7 downto 0);
    signal gt_rxctrl3           : std_logic_vector(7 downto 0);
    signal gt_txctrl2           : std_logic_vector(7 downto 0);

    signal rx_tvalid            : std_logic;
    signal word_cnt             : unsigned(31 downto 0);

begin

    areset              <= not aresetn;
    m_axis_clk          <= clk_gt;
    m_axis_tvalid       <= rx_tvalid;

    ------------------
    -- CLOCK BUFFER --
    ------------------
	BUFG_GT_inst : BUFG_GT
	port map (
		O => clk_gt,
		CE => '1',
		CEMASK => '0',
		CLR => rst_tx_usrclk,
		CLRMASK => '0',
		DIV => "000",
		I => txoutclk
	);

    -----------
    -- RESET --
    -----------
    rst_auroragt        <= reg_gtreset(0);
    rst_aurorasys       <= reg_rst_system(0);
    rst_tx_usrclk       <= not gt_txpmarstdone;
    rst_datapath        <= gt_link_reset or gt_rxreset;

    ---------
    -- CDC --
    ---------
    inst_cdc: xpm_cdc_pulse
    generic map(
        REG_OUTPUT  => 1
    )
    port map(
        src_pulse   => rst_datapath,
        src_clk     => clk_gt,
        src_rst     => rst_tx_usrclk,
        dest_rst    => areset,
        dest_clk    => free_100_clk,
        dest_pulse  => rst_datapath_sp
    );

    -- shaping a >6 clk pulse
    rst_reg_initclk_p:process(aresetn, free_100_clk)
    begin
        if aresetn = '0' then
            reg_gtreset         <= (others => '1');
            rst_gtwiz           <= '1';
        elsif rising_edge(free_100_clk) then
            rst_gtwiz           <= usrrst_gtwizreset;

            if usrrst_gtreset  = '1' then
                reg_gtreset     <= (others => '1');
            else
                reg_gtreset     <= '0' & reg_gtreset(6 downto 1);
            end if;
        end if;
    end process;

    -- CDC shaping a >6 clk pulse
	active_clk_p:process(aresetn, rst_tx_usrclk, clk_gt)
	begin
        if rst_tx_usrclk = '1' or aresetn = '0' then
            reg_clk_active  <= (others => '0');
            reg_rst_system  <= (others => '1');
        elsif rising_edge(clk_gt) then
            reg_clk_active      <= '1' & reg_clk_active(6 downto 1);

            if usrrst_system = '1' then
                reg_rst_system  <= (others => '1');
            else
                reg_rst_system  <= '0' & reg_rst_system(6 downto 1);
            end if;
        end if;
    end process;

    ------------
    -- STATUS --
    ------------
    aurora_reset_lane   <= rst_datapath;
    txpmarstdone        <= gt_txpmarstdone;
    rxpmarstdone        <= gt_rxpmarstdone;
    gtclk_active        <= reg_clk_active(0);
    gtwztxrstdone       <= gt_txrstdone(0);
    gtwzrxrstdone       <= gt_rxrstdone(0);
    word_count          <= std_logic_vector(word_cnt);

    status_p:process(aresetn, clk_gt)
    begin
        if aresetn = '0' then
            word_cnt    <= (others => '0');
        elsif rising_edge(clk_gt) then
            if rx_tvalid = '1' then
                word_cnt <= word_cnt+1;
            end if;
        end if;
    end process;


    --------------------------
    -- AURORA + GT INSTANCE --
    --------------------------
    -- Very crude if generate to select the good IP
    -- I did not find a smarter solution

    G0:if G_IPNUM=0 generate

        inst_aurora_8b10b:comlbp_aurora_0
        port map(
            init_clk_in         => free_100_clk,
            user_clk            => clk_gt,
            sync_clk            => clk_gt,
            txoutclk_in(0)      => clk_gt,
            tx_out_clk          => open,
            txlock_in(0)        => qpll_lock_in,
            -- axis
            m_axi_rx_tdata      => m_axis_tdata,
            m_axi_rx_tvalid     => rx_tvalid,
            s_axi_tx_tdata      => (others => '0'),
            s_axi_tx_tvalid     => '0', -- no TX
            s_axi_tx_tready     => open,
            -- status
            hard_err            => hard_err,
            soft_err            => soft_err,
            channel_up          => channel_up,
            lane_up(0)          => lane_up,
            tx_lock             => tx_lock,
            rxfsm_datavalid_out => aurora_datavalid,
            -- reset
            reset               => rst_aurorasys,
            gt_reset            => rst_auroragt,
            link_reset_out      => gt_link_reset,
            gtrxreset_out       => gt_rxreset,
            gttxresetdone_in    => gt_txrstdone(0),
            gtrxresetdone_in    => gt_rxrstdone(0),
            rxreset_out         => open,
            txreset_out         => open,
            -- Between aurora and GT
            txdata_out          => gt_txdata,
            rxdata_in           => gt_rxdata,
            rxnotintable_in     => gt_rxctrl3(3 downto 0),
            rxdisperr_in        => gt_rxctrl1(3 downto 0),
            rxchariscomma_in    => gt_rxctrl2(3 downto 0),
            rxcharisk_in        => gt_rxctrl0(3 downto 0),
            rxrealign_in        => gt_rxrealign,
            rxbuferr_in(0)      => gt_rxbufstatus(2),
            txbuferr_in(0)      => gt_txbufstatus(1),
            rxpolarity_out      => gt_rxpolarity,
            enacommaalign_out   => gt_enacommaalign,
            txcharisk_out       => gt_txctrl2(3 downto 0),
            -- Unused
            sys_reset_out       => open,
            power_down          => '0',
            enchansync_out      => open,
            chbonddone_in       => "1"
        );
        gt_txctrl2(7 downto 4) <= (others => '0');

        inst_gtwizard:comlbp_gtwizard_0
        port map (
            -- Reset helper block
            gtwiz_reset_clk_freerun_in(0)      => free_100_clk,
            gtwiz_reset_rx_datapath_in(0)      => rst_datapath_sp,
            gtwiz_reset_qpll1lock_in(0)        => qpll_lock_in,
            gtwiz_reset_qpll1reset_out(0)      => qpll_reset_out,
            gtwiz_reset_all_in(0)              => rst_gtwiz,
            -- unused
            gtwiz_reset_tx_pll_and_datapath_in => "0",
            gtwiz_reset_rx_pll_and_datapath_in => "0",
            gtwiz_reset_tx_datapath_in         => "0",
            gtwiz_reset_rx_cdr_stable_out      => open,
            rxcdrlock_out(0)                   => gtcdrstable,
            gtwiz_reset_tx_done_out            => gt_txrstdone,
            gtwiz_reset_rx_done_out            => gt_rxrstdone,
            rxresetdone_out(0)                 => gtrxrstdone,
            txresetdone_out(0)                 => gttxrstdone,
            -- sfp
            gtyrxn_in(0)                       => sfp_rxn,
            gtyrxp_in(0)                       => sfp_rxp,
            gtytxn_out(0)                      => sfp_txn,
            gtytxp_out(0)                      => sfp_txp,
            -- qpll
            qpll1clk_in(0)                     => qpll_clk_in,
            qpll1refclk_in(0)                  => qpll_refclk_in,
            qpll0clk_in                        => "0",
            qpll0refclk_in                     => "0",
            -- clocking
            txoutclk_out(0)                    => txoutclk,
            txusrclk_in(0)                     => clk_gt,
            txusrclk2_in(0)                    => clk_gt,
            rxusrclk_in(0)                     => clk_gt,
            rxusrclk2_in(0)                    => clk_gt,
            gtwiz_userclk_rx_active_in(0)      => reg_clk_active(0),
            gtwiz_userclk_tx_active_in(0)      => reg_clk_active(0),
            -- unused
            rxoutclk_out                       => open,
            -- data helper block
            gtwiz_userdata_rx_out              => gt_rxdata,
            gtwiz_userdata_tx_in               => gt_txdata,
            -- transceiver channel ports
            rxmcommaalignen_in                 => gt_enacommaalign,
            rxpcommaalignen_in                 => gt_enacommaalign,
            rxbyterealign_out                  => gt_rxrealign,
            gtpowergood_out(0)                 => powergood,
            rxbufstatus_out                    => gt_rxbufstatus,
            txbufstatus_out                    => gt_txbufstatus,
            rxctrl0_out                        => gt_rxctrl0,
            rxctrl1_out                        => gt_rxctrl1,
            rxctrl2_out                        => gt_rxctrl2,
            rxctrl3_out                        => gt_rxctrl3,
            rxpmaresetdone_out(0)              => rxpmarstdone,
            txpmaresetdone_out(0)              => gt_txpmarstdone,
            rxpolarity_in                      => gt_rxpolarity,
            txctrl2_in                         => gt_txctrl2,
            loopback_in                        => gtloopback,
            -- unused
            rxpd_in                            => "00",
            txpd_in                            => "00",
            txdetectrx_in                      => "0",
            txelecidle_in                      => "0",
            rxcommadeten_in                    => "1",
            rxbufreset_in                      => "0",
            rx8b10ben_in                       => "1",
            tx8b10ben_in                       => "1",
            rxbyteisaligned_out                => open,
            rxcommadet_out                     => open,
            rxclkcorcnt_out                    => open,
            txctrl0_in                         => (others => '0'),
            txctrl1_in                         => (others => '0')
      );
    end generate G0;

    G1:if G_IPNUM=1 generate

        inst_aurora_8b10b:comlbp_aurora_1
        port map(
            init_clk_in         => free_100_clk,
            user_clk            => clk_gt,
            sync_clk            => clk_gt,
            txoutclk_in(0)      => clk_gt,
            tx_out_clk          => open,
            txlock_in(0)        => qpll_lock_in,
            -- axis
            m_axi_rx_tdata      => m_axis_tdata,
            m_axi_rx_tvalid     => rx_tvalid,
            s_axi_tx_tdata      => (others => '0'),
            s_axi_tx_tvalid     => '0', -- no TX
            s_axi_tx_tready     => open,
            -- status
            hard_err            => hard_err,
            soft_err            => soft_err,
            channel_up          => channel_up,
            lane_up(0)          => lane_up,
            tx_lock             => tx_lock,
            rxfsm_datavalid_out => aurora_datavalid,
            -- reset
            reset               => rst_aurorasys,
            gt_reset            => rst_auroragt,
            link_reset_out      => gt_link_reset,
            gtrxreset_out       => gt_rxreset,
            gttxresetdone_in    => gt_txrstdone(0),
            gtrxresetdone_in    => gt_rxrstdone(0),
            rxreset_out         => open,
            txreset_out         => open,
            -- Between aurora and GT
            txdata_out          => gt_txdata,
            rxdata_in           => gt_rxdata,
            rxnotintable_in     => gt_rxctrl3(3 downto 0),
            rxdisperr_in        => gt_rxctrl1(3 downto 0),
            rxchariscomma_in    => gt_rxctrl2(3 downto 0),
            rxcharisk_in        => gt_rxctrl0(3 downto 0),
            rxrealign_in        => gt_rxrealign,
            rxbuferr_in(0)      => gt_rxbufstatus(2),
            txbuferr_in(0)      => gt_txbufstatus(1),
            rxpolarity_out      => gt_rxpolarity,
            enacommaalign_out   => gt_enacommaalign,
            txcharisk_out       => gt_txctrl2(3 downto 0),
            -- Unused
            sys_reset_out       => open,
            power_down          => '0',
            enchansync_out      => open,
            chbonddone_in       => "1"
        );
        gt_txctrl2(7 downto 4) <= (others => '0');

        inst_gtwizard:comlbp_gtwizard_1
        port map (
            -- Reset helper block
            gtwiz_reset_clk_freerun_in(0)      => free_100_clk,
            gtwiz_reset_rx_datapath_in(0)      => rst_datapath_sp,
            gtwiz_reset_qpll1lock_in(0)        => qpll_lock_in,
            gtwiz_reset_qpll1reset_out(0)      => qpll_reset_out,
            gtwiz_reset_all_in(0)              => rst_gtwiz,
            -- unused
            gtwiz_reset_tx_pll_and_datapath_in => "0",
            gtwiz_reset_rx_pll_and_datapath_in => "0",
            gtwiz_reset_tx_datapath_in         => "0",
            gtwiz_reset_rx_cdr_stable_out      => open,
            rxcdrlock_out(0)                   => gtcdrstable,
            gtwiz_reset_tx_done_out            => gt_txrstdone,
            gtwiz_reset_rx_done_out            => gt_rxrstdone,
            rxresetdone_out(0)                 => gtrxrstdone,
            txresetdone_out(0)                 => gttxrstdone,
            -- sfp
            gtyrxn_in(0)                       => sfp_rxn,
            gtyrxp_in(0)                       => sfp_rxp,
            gtytxn_out(0)                      => sfp_txn,
            gtytxp_out(0)                      => sfp_txp,
            -- qpll
            qpll1clk_in(0)                     => qpll_clk_in,
            qpll1refclk_in(0)                  => qpll_refclk_in,
            qpll0clk_in                        => "0",
            qpll0refclk_in                     => "0",
            -- clocking
            txoutclk_out(0)                    => txoutclk,
            txusrclk_in(0)                     => clk_gt,
            txusrclk2_in(0)                    => clk_gt,
            rxusrclk_in(0)                     => clk_gt,
            rxusrclk2_in(0)                    => clk_gt,
            gtwiz_userclk_rx_active_in(0)      => reg_clk_active(0),
            gtwiz_userclk_tx_active_in(0)      => reg_clk_active(0),
            -- unused
            rxoutclk_out                       => open,
            -- data helper block
            gtwiz_userdata_rx_out              => gt_rxdata,
            gtwiz_userdata_tx_in               => gt_txdata,
            -- transceiver channel ports
            rxmcommaalignen_in                 => gt_enacommaalign,
            rxpcommaalignen_in                 => gt_enacommaalign,
            rxbyterealign_out                  => gt_rxrealign,
            gtpowergood_out(0)                 => powergood,
            rxbufstatus_out                    => gt_rxbufstatus,
            txbufstatus_out                    => gt_txbufstatus,
            rxctrl0_out                        => gt_rxctrl0,
            rxctrl1_out                        => gt_rxctrl1,
            rxctrl2_out                        => gt_rxctrl2,
            rxctrl3_out                        => gt_rxctrl3,
            rxpmaresetdone_out(0)              => rxpmarstdone,
            txpmaresetdone_out(0)              => gt_txpmarstdone,
            rxpolarity_in                      => gt_rxpolarity,
            txctrl2_in                         => gt_txctrl2,
            loopback_in                        => gtloopback,
            -- unused
            rxpd_in                            => "00",
            txpd_in                            => "00",
            txdetectrx_in                      => "0",
            txelecidle_in                      => "0",
            rxcommadeten_in                    => "1",
            rxbufreset_in                      => "0",
            rx8b10ben_in                       => "1",
            tx8b10ben_in                       => "1",
            rxbyteisaligned_out                => open,
            rxcommadet_out                     => open,
            rxclkcorcnt_out                    => open,
            txctrl0_in                         => (others => '0'),
            txctrl1_in                         => (others => '0')
      );
    end generate G1;

    G2:if G_IPNUM=2 generate

        inst_aurora_8b10b:comlbp_aurora_2
        port map(
            init_clk_in         => free_100_clk,
            user_clk            => clk_gt,
            sync_clk            => clk_gt,
            txoutclk_in(0)      => clk_gt,
            tx_out_clk          => open,
            txlock_in(0)        => qpll_lock_in,
            -- axis
            m_axi_rx_tdata      => m_axis_tdata,
            m_axi_rx_tvalid     => rx_tvalid,
            s_axi_tx_tdata      => (others => '0'),
            s_axi_tx_tvalid     => '0', -- no TX
            s_axi_tx_tready     => open,
            -- status
            hard_err            => hard_err,
            soft_err            => soft_err,
            channel_up          => channel_up,
            lane_up(0)          => lane_up,
            tx_lock             => tx_lock,
            rxfsm_datavalid_out => aurora_datavalid,
            -- reset
            reset               => rst_aurorasys,
            gt_reset            => rst_auroragt,
            link_reset_out      => gt_link_reset,
            gtrxreset_out       => gt_rxreset,
            gttxresetdone_in    => gt_txrstdone(0),
            gtrxresetdone_in    => gt_rxrstdone(0),
            rxreset_out         => open,
            txreset_out         => open,
            -- Between aurora and GT
            txdata_out          => gt_txdata,
            rxdata_in           => gt_rxdata,
            rxnotintable_in     => gt_rxctrl3(3 downto 0),
            rxdisperr_in        => gt_rxctrl1(3 downto 0),
            rxchariscomma_in    => gt_rxctrl2(3 downto 0),
            rxcharisk_in        => gt_rxctrl0(3 downto 0),
            rxrealign_in        => gt_rxrealign,
            rxbuferr_in(0)      => gt_rxbufstatus(2),
            txbuferr_in(0)      => gt_txbufstatus(1),
            rxpolarity_out      => gt_rxpolarity,
            enacommaalign_out   => gt_enacommaalign,
            txcharisk_out       => gt_txctrl2(3 downto 0),
            -- Unused
            sys_reset_out       => open,
            power_down          => '0',
            enchansync_out      => open,
            chbonddone_in       => "1"
        );
        gt_txctrl2(7 downto 4) <= (others => '0');

        inst_gtwizard:comlbp_gtwizard_2
        port map (
            -- Reset helper block
            gtwiz_reset_clk_freerun_in(0)      => free_100_clk,
            gtwiz_reset_rx_datapath_in(0)      => rst_datapath_sp,
            gtwiz_reset_qpll1lock_in(0)        => qpll_lock_in,
            gtwiz_reset_qpll1reset_out(0)      => qpll_reset_out,
            gtwiz_reset_all_in(0)              => rst_gtwiz,
            -- unused
            gtwiz_reset_tx_pll_and_datapath_in => "0",
            gtwiz_reset_rx_pll_and_datapath_in => "0",
            gtwiz_reset_tx_datapath_in         => "0",
            gtwiz_reset_rx_cdr_stable_out      => open,
            rxcdrlock_out(0)                   => gtcdrstable,
            gtwiz_reset_tx_done_out            => gt_txrstdone,
            gtwiz_reset_rx_done_out            => gt_rxrstdone,
            rxresetdone_out(0)                 => gtrxrstdone,
            txresetdone_out(0)                 => gttxrstdone,
            -- sfp
            gtyrxn_in(0)                       => sfp_rxn,
            gtyrxp_in(0)                       => sfp_rxp,
            gtytxn_out(0)                      => sfp_txn,
            gtytxp_out(0)                      => sfp_txp,
            -- qpll
            qpll1clk_in(0)                     => qpll_clk_in,
            qpll1refclk_in(0)                  => qpll_refclk_in,
            qpll0clk_in                        => "0",
            qpll0refclk_in                     => "0",
            -- clocking
            txoutclk_out(0)                    => txoutclk,
            txusrclk_in(0)                     => clk_gt,
            txusrclk2_in(0)                    => clk_gt,
            rxusrclk_in(0)                     => clk_gt,
            rxusrclk2_in(0)                    => clk_gt,
            gtwiz_userclk_rx_active_in(0)      => reg_clk_active(0),
            gtwiz_userclk_tx_active_in(0)      => reg_clk_active(0),
            -- unused
            rxoutclk_out                       => open,
            -- data helper block
            gtwiz_userdata_rx_out              => gt_rxdata,
            gtwiz_userdata_tx_in               => gt_txdata,
            -- transceiver channel ports
            rxmcommaalignen_in                 => gt_enacommaalign,
            rxpcommaalignen_in                 => gt_enacommaalign,
            rxbyterealign_out                  => gt_rxrealign,
            gtpowergood_out(0)                 => powergood,
            rxbufstatus_out                    => gt_rxbufstatus,
            txbufstatus_out                    => gt_txbufstatus,
            rxctrl0_out                        => gt_rxctrl0,
            rxctrl1_out                        => gt_rxctrl1,
            rxctrl2_out                        => gt_rxctrl2,
            rxctrl3_out                        => gt_rxctrl3,
            rxpmaresetdone_out(0)              => rxpmarstdone,
            txpmaresetdone_out(0)              => gt_txpmarstdone,
            rxpolarity_in                      => gt_rxpolarity,
            txctrl2_in                         => gt_txctrl2,
            loopback_in                        => gtloopback,
            -- unused
            rxpd_in                            => "00",
            txpd_in                            => "00",
            txdetectrx_in                      => "0",
            txelecidle_in                      => "0",
            rxcommadeten_in                    => "1",
            rxbufreset_in                      => "0",
            rx8b10ben_in                       => "1",
            tx8b10ben_in                       => "1",
            rxbyteisaligned_out                => open,
            rxcommadet_out                     => open,
            rxclkcorcnt_out                    => open,
            txctrl0_in                         => (others => '0'),
            txctrl1_in                         => (others => '0')
      );
    end generate G2;

    G3:if G_IPNUM=3 generate

        inst_aurora_8b10b:comlbp_aurora_3
        port map(
            init_clk_in         => free_100_clk,
            user_clk            => clk_gt,
            sync_clk            => clk_gt,
            txoutclk_in(0)      => clk_gt,
            tx_out_clk          => open,
            txlock_in(0)        => qpll_lock_in,
            -- axis
            m_axi_rx_tdata      => m_axis_tdata,
            m_axi_rx_tvalid     => rx_tvalid,
            s_axi_tx_tdata      => (others => '0'),
            s_axi_tx_tvalid     => '0', -- no TX
            s_axi_tx_tready     => open,
            -- status
            hard_err            => hard_err,
            soft_err            => soft_err,
            channel_up          => channel_up,
            lane_up(0)          => lane_up,
            tx_lock             => tx_lock,
            rxfsm_datavalid_out => aurora_datavalid,
            -- reset
            reset               => rst_aurorasys,
            gt_reset            => rst_auroragt,
            link_reset_out      => gt_link_reset,
            gtrxreset_out       => gt_rxreset,
            gttxresetdone_in    => gt_txrstdone(0),
            gtrxresetdone_in    => gt_rxrstdone(0),
            rxreset_out         => open,
            txreset_out         => open,
            -- Between aurora and GT
            txdata_out          => gt_txdata,
            rxdata_in           => gt_rxdata,
            rxnotintable_in     => gt_rxctrl3(3 downto 0),
            rxdisperr_in        => gt_rxctrl1(3 downto 0),
            rxchariscomma_in    => gt_rxctrl2(3 downto 0),
            rxcharisk_in        => gt_rxctrl0(3 downto 0),
            rxrealign_in        => gt_rxrealign,
            rxbuferr_in(0)      => gt_rxbufstatus(2),
            txbuferr_in(0)      => gt_txbufstatus(1),
            rxpolarity_out      => gt_rxpolarity,
            enacommaalign_out   => gt_enacommaalign,
            txcharisk_out       => gt_txctrl2(3 downto 0),
            -- Unused
            sys_reset_out       => open,
            power_down          => '0',
            enchansync_out      => open,
            chbonddone_in       => "1"
        );
        gt_txctrl2(7 downto 4) <= (others => '0');

        inst_gtwizard:comlbp_gtwizard_3
        port map (
            -- Reset helper block
            gtwiz_reset_clk_freerun_in(0)      => free_100_clk,
            gtwiz_reset_rx_datapath_in(0)      => rst_datapath_sp,
            gtwiz_reset_qpll1lock_in(0)        => qpll_lock_in,
            gtwiz_reset_qpll1reset_out(0)      => qpll_reset_out,
            gtwiz_reset_all_in(0)              => rst_gtwiz,
            -- unused
            gtwiz_reset_tx_pll_and_datapath_in => "0",
            gtwiz_reset_rx_pll_and_datapath_in => "0",
            gtwiz_reset_tx_datapath_in         => "0",
            gtwiz_reset_rx_cdr_stable_out      => open,
            rxcdrlock_out(0)                   => gtcdrstable,
            gtwiz_reset_tx_done_out            => gt_txrstdone,
            gtwiz_reset_rx_done_out            => gt_rxrstdone,
            rxresetdone_out(0)                 => gtrxrstdone,
            txresetdone_out(0)                 => gttxrstdone,
            -- sfp
            gtyrxn_in(0)                       => sfp_rxn,
            gtyrxp_in(0)                       => sfp_rxp,
            gtytxn_out(0)                      => sfp_txn,
            gtytxp_out(0)                      => sfp_txp,
            -- qpll
            qpll1clk_in(0)                     => qpll_clk_in,
            qpll1refclk_in(0)                  => qpll_refclk_in,
            qpll0clk_in                        => "0",
            qpll0refclk_in                     => "0",
            -- clocking
            txoutclk_out(0)                    => txoutclk,
            txusrclk_in(0)                     => clk_gt,
            txusrclk2_in(0)                    => clk_gt,
            rxusrclk_in(0)                     => clk_gt,
            rxusrclk2_in(0)                    => clk_gt,
            gtwiz_userclk_rx_active_in(0)      => reg_clk_active(0),
            gtwiz_userclk_tx_active_in(0)      => reg_clk_active(0),
            -- unused
            rxoutclk_out                       => open,
            -- data helper block
            gtwiz_userdata_rx_out              => gt_rxdata,
            gtwiz_userdata_tx_in               => gt_txdata,
            -- transceiver channel ports
            rxmcommaalignen_in                 => gt_enacommaalign,
            rxpcommaalignen_in                 => gt_enacommaalign,
            rxbyterealign_out                  => gt_rxrealign,
            gtpowergood_out(0)                 => powergood,
            rxbufstatus_out                    => gt_rxbufstatus,
            txbufstatus_out                    => gt_txbufstatus,
            rxctrl0_out                        => gt_rxctrl0,
            rxctrl1_out                        => gt_rxctrl1,
            rxctrl2_out                        => gt_rxctrl2,
            rxctrl3_out                        => gt_rxctrl3,
            rxpmaresetdone_out(0)              => rxpmarstdone,
            txpmaresetdone_out(0)              => gt_txpmarstdone,
            rxpolarity_in                      => gt_rxpolarity,
            txctrl2_in                         => gt_txctrl2,
            loopback_in                        => gtloopback,
            -- unused
            rxpd_in                            => "00",
            txpd_in                            => "00",
            txdetectrx_in                      => "0",
            txelecidle_in                      => "0",
            rxcommadeten_in                    => "1",
            rxbufreset_in                      => "0",
            rx8b10ben_in                       => "1",
            tx8b10ben_in                       => "1",
            rxbyteisaligned_out                => open,
            rxcommadet_out                     => open,
            rxclkcorcnt_out                    => open,
            txctrl0_in                         => (others => '0'),
            txctrl1_in                         => (others => '0')
      );
    end generate G3;

end architecture struct;
