library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library xpm;
use xpm.vcomponents.all;

library desyrdl;
use desyrdl.pkg_comlbp.all;

library comlbp_lib;
use comlbp_lib.pkg_comlbp.all;
use comlbp_lib.pkg_comlbp_version.all;


entity top_comlbp is
    generic(
       G_IPNUM                            : natural :=  0  -- used to select the generated IP
    );
    port(
        aresetn                 : in std_logic;
        free_100_clk            : in std_logic;

        pps                     : in std_logic;

        -- Common Quad signals
        qpll_clk_in             : in std_logic;
        qpll_refclk_in          : in std_logic;
        qpll_lock_in            : in std_logic;
        qpll_reset_out          : out std_logic;
        qpll_fblost_in          : in std_logic;
        qpll_reflost_in         : in std_logic;

        -- SFP interfaces
        sfp_txp                 : out std_logic;
        sfp_txn                 : out std_logic;
        sfp_rxp                 : in std_logic;
        sfp_rxn                 : in std_logic;
        sfp_rx_los              : in std_logic;
        sfp_mod_abs             : in std_logic;
        sfp_tx_disable          : out std_logic;
        sfp_tx_fault            : in std_logic;

        -- debug output
        dbg_data                : out std_logic_vector(31 downto 0);
        dbg_valid               : out std_logic;

        -- AXIS output
        m_axis_aclk             : out std_logic;
        m_axis_tvalid           : out std_logic;
        m_axis_tdata_xpos       : out std_logic_vector(31 downto 0);
        m_axis_tdata_ypos       : out std_logic_vector(31 downto 0);
        m_axis_tdata_faseq      : out std_logic_vector(15 downto 0);
        m_axis_tdata_bpmid      : out std_logic_vector(15 downto 0);

        -- AXI-MM interface
        s_axi_clk               : in std_logic;
        s_axi_m2s               : in  t_comlbp_m2s;
        s_axi_s2m               : out t_comlbp_s2m

    );
end entity top_comlbp;

architecture struct of top_comlbp is

    signal areset       : std_logic;
    signal clk_comlbp   : std_logic;

    signal addrmap_i           : t_addrmap_comlbp_in;
    signal addrmap_o           : t_addrmap_comlbp_out;

    signal rx_data          : std_logic_vector(31 downto 0);
    signal rx_data_valid    : std_logic;

    signal qpll_reset   : std_logic;

    signal psreset      : std_logic;

    signal softerror    : std_logic;
    signal harderror    : std_logic;
    signal laneup       : std_logic;
    signal softerror_sync    : std_logic;
    signal harderror_sync    : std_logic;
    signal laneup_sync       : std_logic;
    signal softerror_stick   : std_logic;
    signal harderror_stick   : std_logic;
    signal lanedown_stick      : std_logic;

    signal pps_resync       : std_logic;

begin

    dbg_data        <= rx_data;
    dbg_valid       <= rx_data_valid;

    sfp_tx_disable  <= addrmap_o.control.txdisable.data(0);
    qpll_reset_out  <= qpll_reset;

    -- Async reset active high from input
    areset  <= not aresetn;

    ----------------------
    -- AXI-MM INTERFACE --
    ----------------------
    inst_aximm: entity desyrdl.comlbp
    port map(
        pi_clock    => s_axi_clk,
        pi_reset    => areset,
        pi_s_top    => s_axi_m2s,
        po_s_top    => s_axi_s2m,
        pi_addrmap  => addrmap_i,
        po_addrmap  => addrmap_o
    );

    addrmap_i.version.data.data     <= C_VERSION;

    addrmap_i.sfp.rxlos.data(0)             <= sfp_rx_los;
    addrmap_i.sfp.modabs.data(0)            <= sfp_mod_abs;
    addrmap_i.sfp.txfault.data(0)           <= sfp_tx_fault;
    addrmap_i.gt_status.qplllock.data(0)    <= qpll_lock_in;
    addrmap_i.gt_status.qpllfblost.data(0)  <= qpll_fblost_in;
    addrmap_i.gt_status.qpllreflost.data(0) <= qpll_reflost_in;
    addrmap_i.gt_status.qpllreset.data(0)   <= qpll_reset;

    ---------
    -- CDC --
    ---------
    inst_cdc_softerror: xpm_cdc_single
    port map(
        src_in      => softerror,
        src_clk     => clk_comlbp,
        dest_clk    => s_axi_clk,
        dest_out    => softerror_sync
    );

    inst_cdc_harderror: xpm_cdc_single
    port map(
        src_in      => harderror,
        src_clk     => clk_comlbp,
        dest_clk    => s_axi_clk,
        dest_out    => harderror_sync
    );

    inst_cdc_laneup: xpm_cdc_single
    port map(
        src_in      => laneup,
        src_clk     => clk_comlbp,
        dest_clk    => s_axi_clk,
        dest_out    => laneup_sync
    );

    -------------------
    -- STICKY STATUS --
    -------------------
    sticky_status_p:process(s_axi_clk, aresetn)
    begin
        if aresetn = '0' then
            softerror_stick <= '0';
            harderror_stick <= '0';
            lanedown_stick <= '0';
        elsif rising_edge(s_axi_clk) then
            if addrmap_o.reset_error.reset.data(0) = '1' then
                softerror_stick <= '0';
                harderror_stick <= '0';
                lanedown_stick <= '0';
            else
                if softerror_sync = '1' then
                    softerror_stick <= '1';
                end if;
                if harderror_sync = '1' then
                    harderror_stick <= '1';
                end if;
                if laneup_sync = '0' then
                    lanedown_stick <= '1';
                end if;
            end if;
        end if;
    end process;

    addrmap_i.aurora_status.softerror.data(0) <= softerror_stick;
    addrmap_i.aurora_status.harderror.data(0) <= harderror_stick;
    addrmap_i.aurora_status.laneup.data(0)  <= laneup_sync;
    addrmap_i.aurora_status.lanedown.data(0)  <= lanedown_stick;

    -----------------
    -- GT + AURORA --
    -----------------
    inst_gtaurora: entity comlbp_lib.comlbp_gtaurora
    generic map(G_IPNUM => G_IPNUM)
    port map(

        free_100_clk                       => free_100_clk,
        aresetn                            => aresetn,

        -- QPLL common
        qpll_clk_in                        => qpll_clk_in,
        qpll_refclk_in                     => qpll_refclk_in,
        qpll_lock_in                       => qpll_lock_in,
        qpll_reset_out                     => qpll_reset,

        -- SFP signals
        sfp_rxn                            => sfp_rxn,
        sfp_rxp                            => sfp_rxp,
        sfp_txn                            => sfp_txn,
        sfp_txp                            => sfp_txp,

        -- AXIS
        m_axis_clk                         => clk_comlbp,
        m_axis_tdata                       => rx_data,
        m_axis_tvalid                      => rx_data_valid,

        -- Status
        powergood                          => addrmap_i.gt_status.powergood.data(0),
        gtclk_active                       => addrmap_i.gt_status.gtclkactive.data(0),
        txpmarstdone                       => addrmap_i.gt_status.txpmaresetdone.data(0),
        rxpmarstdone                       => addrmap_i.gt_status.rxpmaresetdone.data(0),
        gttxrstdone                        => addrmap_i.gt_status.gttxresetdone.data(0),
        gtrxrstdone                        => addrmap_i.gt_status.gtrxresetdone.data(0),
        gtcdrstable                        => addrmap_i.gt_status.cdrstable.data(0),
        hard_err                           => harderror,
        soft_err                           => softerror,
        channel_up                         => addrmap_i.aurora_status.channelup.data(0),
        lane_up                            => laneup,
        aurora_datavalid                   => addrmap_i.aurora_status.datavalid.data(0),
        aurora_reset_lane                  => addrmap_i.aurora_status.resetlane.data(0),
        tx_lock                            => addrmap_i.aurora_status.txlock.data(0),
        word_count                         => addrmap_i.word_count.data.data,

        -- Control
        gtloopback                         => addrmap_o.control.gtloopback.data,
        usrrst_system                      => addrmap_o.control.rstsystem.data(0),
        usrrst_gtwizreset                  => addrmap_o.control.rstgtwiz.data(0),
        usrrst_gtreset                     => addrmap_o.control.rstgt.data(0)
    );


    --------------
    -- PROTOCOL --
    --------------
    m_axis_aclk <= clk_comlbp;

    psreset <= addrmap_o.control.rstsystem.data(0);

    inst_protocol: entity comlbp_lib.comlbp_protocol
    port map(
        aresetn             => aresetn,
        clk                 => clk_comlbp,
        pps                 => pps_resync,

        s_axis_tdata        => rx_data,
        s_axis_tvalid       => rx_data_valid,

        sreset              => psreset,
        enable              => addrmap_o.control.enable.data(0),
        packet_count        => addrmap_i.packet_count.data.data,
        faseq_offset        => signed(addrmap_o.SEQ_OFFSET.data.data),
        packet_rate         => addrmap_i.packet_rate.data.data,

        m_axis_tvalid       => m_axis_tvalid,
        m_axis_tdata_xpos   => m_axis_tdata_xpos,
        m_axis_tdata_ypos   => m_axis_tdata_ypos,
        m_axis_tdata_faseq  => m_axis_tdata_faseq,
        m_axis_tdata_bpmid  => m_axis_tdata_bpmid
    );

    -------------
    -- PPS CDC --
    -------------
    xpm_cdc_pps_inst: xpm_cdc_pulse
    generic map (
        RST_USED => 0,
        DEST_SYNC_FF => 4,
        INIT_SYNC_FF => 0
    )
    port map (
        dest_clk    => clk_comlbp,
        dest_pulse  => pps_resync,
        src_rst     => '0',
        dest_rst    => '0',
        src_clk     => free_100_clk,
        src_pulse   => pps
    );

end architecture struct;

