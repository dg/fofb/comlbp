# This script creates one GTWizard transceiver module
# This generate for a GTY transceiver, one would need to customize to generate for GTH

# Used variables, must be declared
# $idx : suffixes names, used for multiple instances
# $GT_LOC : Loc name of the GT, ie "X0Y4"


set ProjectDirPath [file join ${::fwfwk::PrjBuildPath} ${::fwfwk::PrjBuildName}]

## ------------------------------------- ##
## GT WIZARD
## ------------------------------------- ##
set ipName "comlbp_gtwizard_$idx"

set xcipath [create_ip \
    -name gtwizard_ultrascale \
    -vendor xilinx.com \
    -library ip -version 1.7 \
    -module_name $ipName]

# We need to put first the preset, then customize
set_property -dict [list \
    CONFIG.preset {GTY-Aurora_8B10B} \
    ] [get_ips $ipName]

set ipProp [list \
    CONFIG.CHANNEL_ENABLE $GT_LOC \
    CONFIG.LOCATE_COMMON {EXAMPLE_DESIGN} \
    CONFIG.TX_LINE_RATE {6.5} \
    CONFIG.TX_PLL_TYPE {QPLL1} \
    CONFIG.TX_REFCLK_FREQUENCY {156.25} \
    CONFIG.TX_USER_DATA_WIDTH {32} \
    CONFIG.TX_INT_DATA_WIDTH {40} \
    CONFIG.TX_QPLL_FRACN_NUMERATOR {3355443} \
    CONFIG.RX_LINE_RATE {6.5} \
    CONFIG.RX_PLL_TYPE {QPLL1} \
    CONFIG.RX_REFCLK_FREQUENCY {156.25} \
    CONFIG.RX_USER_DATA_WIDTH {32} \
    CONFIG.RX_INT_DATA_WIDTH {40} \
    CONFIG.RX_QPLL_FRACN_NUMERATOR {3355443} \
    CONFIG.RX_JTOL_FC {3.8992202} \
    CONFIG.RX_CB_NUM_SEQ {0} \
    CONFIG.RX_CB_MAX_SKEW {1} \
    CONFIG.RX_CB_VAL_0_0 {00000000} \
    CONFIG.RX_CB_K_0_0 {false} \
    CONFIG.RX_REFCLK_SOURCE "${GT_LOC} clk0" \
    CONFIG.TX_REFCLK_SOURCE "${GT_LOC} clk0" \
    CONFIG.LOCATE_TX_USER_CLOCKING {EXAMPLE_DESIGN} \
    CONFIG.LOCATE_RX_USER_CLOCKING {EXAMPLE_DESIGN} \
    CONFIG.LOCATE_RESET_CONTROLLER {CORE} \
    CONFIG.LOCATE_USER_DATA_WIDTH_SIZING {CORE} \
    CONFIG.TXPROGDIV_FREQ_SOURCE {QPLL1} \
    CONFIG.TXPROGDIV_FREQ_VAL {162.5} \
    CONFIG.FREERUN_FREQUENCY {100} \
    CONFIG.RX_COMMA_ALIGN_WORD {2} \
    CONFIG.RX_CC_LEN_SEQ 4 \
    CONFIG.RX_CC_PERIODICITY {2500} \
    CONFIG.RX_CC_VAL 00000000000000000000000000000000000000000011110111001111011100111101110011110111 \
    CONFIG.RX_CC_VAL_0_1 {11110111} \
    CONFIG.RX_CC_VAL_0_2 {11110111} \
    CONFIG.RX_CC_VAL_0_3 {11110111} \
    CONFIG.RX_CC_K_0_1 {true} \
    CONFIG.RX_CC_K_0_2 {true} \
    CONFIG.RX_CC_K_0_3 {true} \
    CONFIG.ENABLE_OPTIONAL_PORTS {loopback_in rxpolarity_in txbufstatus_out txoutclk_out \
        rxresetdone_out txresetdone_out txdetectrx_in txelecidle_in rxpd_in txpd_in rxcdrlock_out } \
    CONFIG.SECONDARY_QPLL_ENABLE true \
    CONFIG.SECONDARY_QPLL_FRACN_NUMERATOR 0 \
    CONFIG.SECONDARY_QPLL_LINE_RATE 10.3125 \
    CONFIG.SECONDARY_QPLL_REFCLK_FREQUENCY 156.25 \
    ]

set_property -dict $ipProp [get_ips $ipName]

generate_target all [get_files  ${ProjectDirPath}.srcs/sources_1/ip/$ipName/$ipName.xci]
export_ip_user_files -of_objects [get_files ${ProjectDirPath}.srcs/sources_1/ip/$ipName/$ipName.xci] -no_script -sync -force -quiet
create_ip_run [get_files -of_objects [get_fileset sources_1] ${ProjectDirPath}.srcs/sources_1/ip/$ipName/$ipName.xci]

## ------------------------------------- ##
## AURORA 8B/10B
## ------------------------------------- ##
set ipName "comlbp_aurora_$idx"

set xcipath [create_ip \
    -name aurora_8b10b \
    -vendor xilinx.com \
    -library ip -version 11.1 \
    -module_name $ipName]

# Configuration about the GT will be ignored, GT wizard is generated outside of the core.
set ipProp [list \
    CONFIG.C_LANE_WIDTH {4} \
    CONFIG.C_LINE_RATE {6.5} \
    CONFIG.C_REFCLK_FREQUENCY {162.5} \
    CONFIG.C_INIT_CLK {100} \
    CONFIG.Dataflow_Config {Duplex} \
    CONFIG.Interface_Mode {Streaming} \
    CONFIG.C_START_QUAD {Quad_X0Y1} \
    CONFIG.C_START_LANE $GT_LOC \
    CONFIG.C_REFCLK_SOURCE {MGTREFCLK0 of Quad X0Y1} \
    CONFIG.CHANNEL_ENABLE $GT_LOC \
    CONFIG.C_GTWIZ_OUT {true} \
    ]

set_property -dict $ipProp [get_ips $ipName]

generate_target all [get_files  ${ProjectDirPath}.srcs/sources_1/ip/$ipName/$ipName.xci]
export_ip_user_files -of_objects [get_files ${ProjectDirPath}.srcs/sources_1/ip/$ipName/$ipName.xci] -no_script -sync -force -quiet
create_ip_run [get_files -of_objects [get_fileset sources_1] ${ProjectDirPath}.srcs/sources_1/ip/$ipName/$ipName.xci]
