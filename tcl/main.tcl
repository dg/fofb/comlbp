################################################################################
# Main tcl for the module
################################################################################

# ==============================================================================
proc init {} {
    variable Config
}

# ==============================================================================
proc setSources {} {
  variable Sources

  # Generate VHDL package with modversion
  genModVerFile VHDL ../hdl/pkg_comlbp_version.vhd

  lappend Sources {"../hdl/pkg_comlbp_version.vhd"      "VHDL 2008"     "comlbp_lib"}
  lappend Sources {"../hdl/pkg_comlbp.vhd"              "VHDL 2008"     "comlbp_lib"}
  lappend Sources {"../hdl/comlbp_gtaurora.vhd"         "VHDL 2008"     "comlbp_lib"}
  lappend Sources {"../hdl/comlbp_protocol.vhd"         "VHDL 2008"     "comlbp_lib"}
  lappend Sources {"../hdl/top_comlbp.vhd"              "VHDL 2008"     "comlbp_lib"}
}

# ==============================================================================
proc setAddressSpace {} {
    variable AddressSpace
    addAddressSpace AddressSpace "comlbp" RDL {} ../rdl/comlbp.rdl
}

# ==============================================================================
proc doOnCreate {} {
  variable Sources
  variable Config

  # This module is only for vivado tool. End here for another tooltype.
  if {$::fwfwk::ToolType ne "vivado"} {
      puts "\n[ERROR] This module is only configured to work with Vivado"
      exit -1
  }

  set idx 0
  foreach GT_LOC $Config(GT_LOC) {
      source generate_xilinx_ip.tcl
      incr idx
  }

  addSources Sources
}

# ==============================================================================
proc doOnBuild {} {
}

# ==============================================================================
proc setSim {} {
}
